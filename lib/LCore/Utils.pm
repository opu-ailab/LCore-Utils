package LCore::Utils;
use strict;
use warnings;
our $VERSION = '0.01';

use base qw(Exporter);
our @EXPORT = qw(lcore_system lcore_system_msg lcore_fopen_msg);

sub lcore_system {
    my ($msg) = @_;
    if (exists($ENV{"LCORE_LOG_LEVEL"}) && $ENV{"LCORE_LOG_LEVEL"} eq "info") {
        print STDERR "EXEC: ", $msg, "\n";
    }
    return system($msg);
}

sub lcore_system_msg {
    my ($msg) = @_;
    if (exists($ENV{"LCORE_LOG_LEVEL"}) && $ENV{"LCORE_LOG_LEVEL"} eq "info") {
        print STDERR "EXEC: ", $msg, "\n";
    }
    1;
}

sub lcore_fopen_msg {
    my ($msg) = @_;
    if (exists($ENV{"LCORE_LOG_LEVEL"}) && $ENV{"LCORE_LOG_LEVEL"} eq "info") {
        print STDERR "OPEN: ", $msg, "\n";
    }
    1;
}

1;
__END__

=head1 NAME

LCore::Utils -

=head1 SYNOPSIS

  use LCore::Utils;

=head1 DESCRIPTION

LCore::Utils is

=head1 AUTHOR

Takeo Kunishima E<lt>t.kunishi@gmail.comE<gt>

=head1 SEE ALSO

=head1 LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut
