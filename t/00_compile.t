use strict;
use Test::More tests => 7;
use Test::Output;

BEGIN { use_ok 'LCore::Utils' }

combined_is {
    delete($ENV{"LCORE_LOG_LEVEL"});
    \&LCore::Utils::lcore_system("/bin/echo test")
} "test\n", "lcore_system() does not return test output.";

combined_is {
    delete($ENV{"LCORE_LOG_LEVEL"});
    $ENV{"LCORE_LOG_LEVEL"} = "info";
    \&LCore::Utils::lcore_system("/bin/echo test")
} "EXEC: /bin/echo test\ntest\n", "lcore_system() returns test output to stderr.";

stderr_is { 
    delete($ENV{"LCORE_LOG_LEVEL"});
    \&LCore::Utils::lcore_system_msg("ls -l")
} "", "lcore_system_msg() does not return test output.";

stderr_is {
    delete($ENV{"LCORE_LOG_LEVEL"});
    $ENV{"LCORE_LOG_LEVEL"} = "info";
    \&LCore::Utils::lcore_system_msg("ls -l")
} "EXEC: ls -l\n", "lcore_system_msg() returns test output.";

stderr_is {
    delete($ENV{"LCORE_LOG_LEVEL"});
    \&LCore::Utils::lcore_fopen_msg(">foo.txt")
} "", "lcore_fopen_msg() does not return test output.";

stderr_is {
    delete($ENV{"LCORE_LOG_LEVEL"});
    $ENV{"LCORE_LOG_LEVEL"} = "info";
    \&LCore::Utils::lcore_fopen_msg(">foo.txt")
} "OPEN: >foo.txt\n", "lcore_fopen_msg() returns test output.";
